#!/bin/bash
echo "Цель скрипта сохранение списка пакетов установленых в системе" & sleep 5s
pacman -Qe > ~/pkglist.edit
#Удаление версии пакетов
echo "line with spaces" | cut -d' ' -f1 ~/pkglist.edit >> ~/pkglist.all
#Удаление
rm ~/pkglist.edit
echo "Получили список пакетов, которые были установлены и через AUR и через репозитория"
echo "Получаем список пакетов, которые были установлены через репозитория"
pacman -Qqe | grep -v "$(pacman -Qmq)" > ~/pkglist.repo
echo "Получаем список пакетов, которые были установление через АУР"
diff --left-column  --unchanged-group-format='' ~/pkglist.all ~/pkglist.repo >> ~/pkglist.aur
echo "В итоге Вы получили три файла:"
echo -e "pkglist.\e[1;31mall\033[0m - Все пакеты"
echo  "И по отдельности:"
echo -e "pkglist.\e[0;32mrepo\033[0m - Пакеты, которые были установлены через репозитория"
echo -e "pkglist.\e[1;33maur\033[0m - Пакеты, которые были установлены через AUR"
echo -e "\e[1;33mПРИМЕЧАНИЕ:\033[0m Пакеты, которые были установлены локально причисляются к списку pkglist.aur"
echo "Удачи!!!"
exit 0